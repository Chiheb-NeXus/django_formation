 
# Installer  django et l'environnement virtuel

## Installer virtualenv:

```python
$ pip install virtualenv
````
vérifier que virtualenv a été bien installé
```python
$ virtualenv --version
````
## Création de l'environnment virtuel
```python 
$ virtualenv formation
````
---
## Activer l'environnement virtuel
```python 
$ cd /formation/bin
$ source activate
$ cd ..
```
## Installer django
```python
$ pip install django 
```

vérifier que django a été bien installé
```python
$ django-admin --version
````
---

# Création d'un nouveau projet django
```python
$ django-admin startproject django_formation
$ cd /django_formation
````
---
```bash
(formation) user@name: ~/formation/django_formation: 
$ tree

├── db.sqlite3
├── django_formation
│   ├── __init__.py
│   ├── __pycache__
│   │   ├── __init__.cpython-35.pyc
│   │   ├── settings.cpython-35.pyc
│   │   ├── urls.cpython-35.pyc
│   │   └── wsgi.cpython-35.pyc
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
└── manage.py
```
---
# Tester notre projet
```bash
(formation) user@name: ~/formation/django_formation
$ python manage runserver
```
---
![](/Users/mounir/Desktop/projet.png)

---

# Création de l'application blog sous notre projet
```python
(formation) user@name: ~/formation/django_formation:
$ python manage.py startapp blog
$ cd /blog
```
---

```bash
(formation) user@name: ~/formation/django_formation: 
$ tree
.
├── blog
│   ├── __init__.py
│   ├── admin.py
│   ├── apps.py
│   ├── migrations
│   │   └── __init__.py
│   ├── models.py
│   ├── tests.py
│   └── views.py
├── django_formation
│   ├── __init__.py
│   ├── __pycache__
│   │   ├── __init__.cpython-35.pyc
│   │   └── settings.cpython-35.pyc
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
└── manage.py
```
---
# Configuration de notre projet
```bash
(formation) user@name: ~/formation/django_formation/
```
- Création des dossiers static,média et template
```bash
$ mkdir static
$ mkdir media
$ mkdir blog/templates
```
- Dans le fichier settings.py sous le répertoire
```bash
(formation) user@name: ~/formation/django_formation
/django_formation
```

---
1. Ajouter notre applicatin "blog" dans les applications installés de Django
```python
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'blog'
]
```
2. Indiquez à Django le dossier templates :
```python 
    TEMPLATES = [
  {		...
        'DIRS': [os.path.join(BASE_DIR, 'blog/templates')],
      ...
  }
]
```

---
3. Les connecteurs de la base de donnée :
	Par default Django utlise `SQLite`
```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
```
4. Les paramétres de la langue et le fuseau horaire :
```python
LANGUAGE_CODE = 'fr-fr'
TIME_ZONE = 'Africa/Tunis'
```
---
5. les paramétres des fichiers statique et média
	* les fichiers statique eg(css,js)
	* media eg(image upload) 
```python

# Static & media files
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static'),
]
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
```

<!--
page_number : true
-->