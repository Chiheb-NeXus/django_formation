
from sub_main import sub_main
from folder.sub_folder_file import sub_folder
from folder.second_sub_folder.second_sub_folder_file import second_sub_folder 

def main():
    sub_main()
    sub_folder()
    second_sub_folder()


if __name__ == '__main__':
    main()