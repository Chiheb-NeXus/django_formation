from couleur import Couleur

class Produit:
    def __init__(self, nom='', descriptif='', couleur='', prix=0.0):
        if not nom:
            raise Exception('Nom est requis')
        elif not descriptif:
            raise Exception('Descriptif est requis')

        self.__nom = nom
        self.__descriptif = descriptif
        self.__prix = prix
        
        if isinstance(couleur, Couleur):
            _rgb, _hex = couleur.get_rgb(), couleur.get_hex()
            self.__couleur = _rgb if _rgb else _hex
        elif isinstance(couleur, tuple):
            self.__couleur = couleur
        elif isinstance(couleur, list):
            self.__couleur = tuple(couleur)
        else:
            raise Exception('Couleur must be tuple, list or instance of Couleur')

    def __str__(self):
        return '{0}({1}) - {2}'.format(self.__nom, ', '.join(map(str, self.__couleur)), self.__prix)

    def __repr__(self):
        return '{0}({1}) - {2}'.format(self.__nom, self.__couleur, self.__prix)

    def set_values(self, nom='', descriptif='', couleur='', prix=0.0):
        assert nom
        assert descriptif
        assert couleur
        self.__nom, self.__descriptif, self.__prix, self.__couleur = nom, descriptif, prix, couleur

    def get_nom(self):
        return self.__nom

    def get_descriptif(self):
        return self.__descriptif

    def get_prix(self):
        return self.__prix

    def get_couleur(self):
        if isinstance(self.__couleur, tuple):
            return self.__couleur
        elif isinstance(self.__couleur, str):
            return '({})'.format(self.__couleur)
        else:
            raise Exception('Cannot format couleur')

if __name__ == '__main__':
    c = Couleur(rgb=(10,10,10))
    a = Produit(nom='gazouz', descriptif='gazouzzz', prix=10, couleur=c)
    print(a)
    a.set_values('hello', 'hello', c)
    print(a)
