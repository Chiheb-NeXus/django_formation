from produit import Produit
from couleur import Couleur
from collections import defaultdict
import random

def reduction(func):
    def wrap(self, *args, **kwargs):
        print('Somme avant réduction:')
        somme = func(self, *args, **kwargs)
        print(somme)
        somme -= somme /10
        return somme
    return wrap


class Client:
    def __init__(self, age=None, gender=None, produits=[]):
        self.age = age
        self.gender = gender
        self.produits = produits

    def __repr__(self):
        return 'Nbr Produit(s): {0} - Somme: {1}'.format(len(self.produits), self.get_sum())

    def __iter__(self):
        for prod in self.__remove_duplications().items():
            yield prod
            
    @reduction
    def get_sum(self):
        return sum(map(lambda x: x.get_prix(), self.produits))

    def get_details(self):
        return ' \n'.join(map(lambda x: '{0}{1} - {2}'.format(x.get_nom(), x.get_couleur(), x.get_prix()), self.produits))

    def __remove_duplications(self):
        a = defaultdict(int)
        for elm in self.produits:
            a[elm.get_nom()] += elm.get_prix()
        return dict(a)

def main():
    randomize = lambda x: random.choice(x)
    randint = lambda x, y: random.randint(x, y)


    prods = ('gazouz', 'eau', 'cafe')
    desc = ('gaz', 'eau', 'noir')
    couleurs = ((0, 102, 255), (102, 51, 0), '#006600')

    elms = [Produit(nom=randomize(prods), descriptif=randomize(desc),
            prix=randint(1, 200), couleur=Couleur(randomize(couleurs)))
            for _ in range(5)]

    c = Client()
    print('Partie 1: Créer un objet')
    print(c)
    # Attendre que l'utilisateur appuie sur Entrer
    input()
    c = Client(12, 'M', [randomize(elms) for _ in range(randint(1, 20))])
    print("Partie 2: Passer des arguments à un objet et l'afficher")
    print(c)
    input()
    print("Partie 3: Appeler une méthode publique d'un objet")
    print(c.get_details())
    input()
    print("Partie 4: Iteration d'un object")
    print("Afficher l'objet: ", c)
    print("Itération d'un objet itérable: ", list(c))


if __name__ == '__main__':
    main()
