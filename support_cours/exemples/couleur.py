class Couleur:
    def __init__(self, rgb=(), _hex=''):
        if not rgb and not _hex:
            raise Exception('At least on of RGB and _hex must be not null arguments')

        self.__rgb = rgb
        self.__hex = _hex

    def __str__(self):
        return '{}'.format(', '.join(map(str, self.__rgb)) if self.__rgb else str(self.__hex))

    def __repr__(self):
        return '{}'.format(', '.join(map(str, self.__rgb)) if self.__rgb else str(self.__hex))

    def __iter__(self):
        for k in (self.__rgb or self.__hex):
            yield k

    def toHEX(self):
        assert self.__rgb
        return '#%02x%02x%02x' % self.__rgb

    def toRGB(self):
        assert self.__hex
        value = self.__hex.lstrip('#')
        lv = len(value)
        return (int(value[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))

    def get_rgb(self):
        return self.__rgb

    def get_hex(self):
        return self.__hex


if __name__ == '__main__':
    a = Couleur(_hex='#1d1d1d')
    print(a)
    print(tuple(a.toRGB()))
