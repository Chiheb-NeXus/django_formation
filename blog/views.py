from django.shortcuts import render, Http404, redirect
from django.http import HttpResponse,HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate, login , logout
from django.contrib.auth.models import User
from django.views import View
from django.db.utils import IntegrityError
from .models import Utilisateur
from .forms.form_login import Form_login,Form_register
# Create your views here.

class Login(View):
	
	form = Form_login
	def get(self,request):
		template = "login.html"
		self.form = Form_login()
		return render(request,template,{'form': self.form})

	def post(self,request):
		form = self.form(request.POST)
		if form.is_valid():
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			user = authenticate(username=username, password=password)
			login(request, user)
			if user:
    				print("user valid")
    				return redirect(reverse('index'))
			else:
    				return HttpResponse("echec")

class Register(View):
	form = Form_register
	template = "register.html"
	def get(self,request):
		self.form = Form_register()
		return render(request,self.template,{'form':self.form})
	def post(self,request):
		form = self.form(request.POST)
		if form.is_valid():
			try:
				username = form.cleaned_data['username']
				password = form.cleaned_data['password']
				nom_prenom = form.cleaned_data['nom']
				form.clean()
				client = User.objects.create_user(username=username, password=password)
				Utilisateur(user=client,nom_prenom=nom_prenom).save()
			except IntegrityError as e:
				print(e)
				return HttpResponse(e)
			return redirect(reverse('index'))
		else:
			print("error")
			return render(request,self.template, {'form':form})

class Index(View):
	template = "index.html"
	
	def get(self,request):
		current_user = request.user
		if current_user.is_authenticated:
			current_user = Utilisateur.objects.get(user = current_user)
			print(current_user.user.username)
			return render(request,self.template, {'current_user':current_user})
		else:
			return redirect(reverse('login') )

class Logout(View):

	def get(self,request):

		logout(request)
		return redirect(reverse('login'))