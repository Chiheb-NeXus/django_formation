from django import forms
from django.utils.translation import gettext as _

class Form_login(forms.Form):
	username = forms.CharField(
	required = True,
	label = "username",
	widget =  forms.TextInput(attrs={'class': "login-input", 'id': 'username_login'}),
 )
	password = forms.CharField(
		label = 'Password',
		max_length = 32,
		widget = forms.PasswordInput(
			attrs={'class' :"login-input", 'id': 'form_login_password'}),
		)


class Form_register(forms.Form):
	nom = forms.CharField(
	required = True,
	label = 'nom et prenom',
	max_length = 200,
	widget = forms.TextInput(attrs={'class': "register-input", 'id': 'address'}),
)
	username = forms.CharField(
	required = True,
	label = 'username',
	max_length = 12,
	widget = forms.TextInput(attrs={'class': "register-input"}),
 )
	password = forms.CharField(
		label = 'Password',
		max_length = 32,
		widget = forms.PasswordInput(
			attrs={'class' : "register-input", 'id': 'form_login_password'}),
		)
	confirm_password = forms.CharField(
		label = _('Confirm Password'),
		max_length = 32,
		widget = forms.PasswordInput(
			attrs={'class' : "register-input", 'id': 'form_register_confirm_password'}),
		)
	def clean(self):

		cleaned_data = super(Form_register, self).clean()
		password = cleaned_data.get("password")
		confirm_password = cleaned_data.get("confirm_password")
		msg = "dddd"
		if password != confirm_password:
			raise forms.ValidationError("password and confirm_password does not match")




