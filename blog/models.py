from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext as _

class Utilisateur(models.Model):
	user = models.OneToOneField(User,on_delete=models.CASCADE)
	nom_prenom = models.CharField(max_length= 200,verbose_name= _("prenom"))


	def __str__(self):
		return '{0} username'.format(self.user.username)

